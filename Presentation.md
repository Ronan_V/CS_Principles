# adafruit Presentation

## Microcontroller

- Definition: A microcontroller is a small integrated circuit that controls a single operation in an embedded system. On a single chip, a typical microcontroller contains a processor, memory, and input/output (I/O) peripherals.

## Circuit Playground

- Circuit Playground features an ATmega32u4 micro-processor. It also is round and has alligator-clip pads around it. You can power it from USB, a AAA battery pack, or Lipoly (for advanced users). Program your code into it, then take it on the go.

Includes:

- ATmega32u4 Processor, running at 3.3V and 8MHz
- MicroUSB port for programming and debugging with Arduino IDE
- USB port can act like serial port, keyboard, mouse, joystick or MIDI
## Twinkle Twinkle Little Star

- We used the adafruit software to produce a variety of sounds on the circuit playground. Using the javascript block code, we were able to manipulate these sounds into music. The song we selected to play on our circuit playground was Twinkle Twinkle Little Star. Upon activating the code, the circuit playground express starts flashing several colored lights, whilst producing the melody. The code we used to create this beautiful tune is displayed below:

input.onSwitchMoved(SwitchDirection.Right, function () {
   music.setVolume(255)
   light.stopAllAnimations()
   while (input.switchRight()) {
       Play_twinkle_twinkle()
   }
})
function Play_Note_line_1 () {
   music.playTone(262, music.beat(BeatFraction.Whole))
   light.showRing(
   `white blue white blue white white blue white blue white`
   )
   music.playTone(262, music.beat(BeatFraction.Whole))
   light.showRing(
   `blue white blue white white blue white blue white white`
   )
   music.playTone(392, music.beat(BeatFraction.Whole))
   light.showRing(
   `white blue white white blue white blue white white blue`
   )
   music.playTone(392, music.beat(BeatFraction.Whole))
   light.showRing(
   `blue white white blue white blue white white blue white`
   )
   music.playTone(440, music.beat(BeatFraction.Whole))
   light.showRing(
   `white white blue white blue white white blue white blue`
   )
   music.playTone(440, music.beat(BeatFraction.Whole))
   light.showRing(
   `white blue white blue white white blue white blue white`
   )
   music.playTone(392, music.beat(BeatFraction.Whole))
   light.showRing(
   `blue white blue white white blue white blue white white`
   )
}
function Play_Note_line_2 () {
   music.playTone(349, music.beat(BeatFraction.Whole))
   light.showRing(
   `white blue white white blue white blue white white blue`
   )
   music.playTone(349, music.beat(BeatFraction.Whole))
   light.showRing(
   `blue white white blue white blue white white blue white`
   )
   music.playTone(330, music.beat(BeatFraction.Whole))
   light.showRing(
   `white white blue white blue white white blue white blue`
   )
   music.playTone(330, music.beat(BeatFraction.Whole))
   light.showRing(
   `white blue white blue white white blue white blue white`
   )
   music.playTone(294, music.beat(BeatFraction.Whole))
   light.showRing(
   `blue white blue white white blue white blue white white`
   )
   music.playTone(294, music.beat(BeatFraction.Whole))
   light.showRing(
   `white blue white white blue white blue white white blue`
   )
   music.playTone(262, music.beat(BeatFraction.Whole))
   light.showRing(
   `blue white white blue white blue white white blue white`
   )
}
function Play_Note_Line_3 () {
   music.playTone(392, music.beat(BeatFraction.Whole))
   light.showRing(
   `white white blue white blue white white blue white blue`
   )
   music.playTone(392, music.beat(BeatFraction.Whole))
   light.showRing(
   `white blue white blue white white blue white blue white`
   )
   music.playTone(349, music.beat(BeatFraction.Whole))
   light.showRing(
   `blue white blue white white blue white blue white white`
   )
   music.playTone(349, music.beat(BeatFraction.Whole))
   light.showRing(
   `white blue white white blue white blue white white blue`
   )
   music.playTone(330, music.beat(BeatFraction.Whole))
   light.showRing(
   `blue white white blue white blue white white blue white`
   )
   music.playTone(330, music.beat(BeatFraction.Whole))
   light.showRing(
   `white white blue white blue white white blue white blue`
   )
   music.playTone(294, music.beat(BeatFraction.Whole))
   light.showRing(
   `white blue white blue white white blue white blue white`
   )
}
function Play_twinkle_twinkle () {
   light.showRing(
   `white white white white white white white white white white`
   )
   Play_Note_line_1()
   Play_Note_line_2()
   Play_Note_Line_3()
   Play_Note_Line_3()
   Play_Note_line_1()
}
forever(function () {
   light.setBrightness(53)
})

## Alohomora Minigame from Hogwart's Legacy

- The second thing we used the adafruit software for is the creation of a minigame found in the video game Hogwart's legacy. This minigame involves selecting random buttons until they match up with the given button in the code. Once one button matches up, half of the playground circuit express lights up. Once both buttons match up, the whole playground circuit express lights up and a victory animation is displayed. Here is the code: 

let num = 0
let tum = 0
let tumeql = 0
let numeql = 0
input.buttonA.onEvent(ButtonEvent.LongClick, function () {
   music.setVolume(0)
   light.stopAllAnimations()
})
input.onSwitchMoved(SwitchDirection.Left, function () {
   music.setVolume(0)
   light.clear()
   light.stopAllAnimations()
   num = 0
   tum = 0
   tumeql = Math.randomRange(1, 10)
   numeql = Math.randomRange(1, 10)
   while (true) {
       if (input.buttonB.isPressed()) {
           tum = tum + 1
       }
       if (input.buttonA.isPressed()) {
           num = num + 1
       }
       if (num == numeql) {
           light.showRing(
           `red red red red red black black black black black`
           )
       }
       if (tum == tumeql) {
           light.showRing(
           `black black black black black blue blue blue blue blue`
           )
       }
       if (tum == tumeql && num == numeql) {
           light.stopAllAnimations()
           light.clear()
           light.showRing(
           `red red red red red blue blue blue blue blue`
           )
           light.showRing(
           `black black black black black black black black black black`
           )
           light.showRing(
           `red red red red red blue blue blue blue blue`
           )
           light.showRing(
           `black black black black black black black black black black`
           )
           light.showRing(
           `red red red red red blue blue blue blue blue`
           )
           light.showAnimation(light.colorWipeAnimation, 2000)
           light.stopAllAnimations()
           light.clear()
           num = 0
       }
   }
})
input.onGesture(Gesture.Shake, function () {
   tum = 0
   num = 0
})
forever(function () {
   light.setBrightness(10)
})




