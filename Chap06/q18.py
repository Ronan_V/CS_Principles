def square(turtle, distance, angle, color1, color2, color3, color4):
    turtle.color(color1)
    turtle.forward(distance)
    turtle.left(angle)
    turtle.color(color2)
    turtle.forward(distance)
    turtle.left(angle)
    turtle.color(color3)
    turtle.forward(distance)
    turtle.left(angle)
    turtle.color(color4)
    turtle.forward(distance)

from turtle import *
Ronan = Turtle()
space = Screen()
square(Ronan, 100, 90, "red", "yellow", "green", "blue")
