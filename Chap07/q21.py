def strangefunction(integer_param):
    numlist = range (1,integer_param + 1)
    evenproduct = 1
    oddsum = 0
    for n in numlist:
        if (n % 2) == 0:
            evenproduct = evenproduct * n
        else:
            oddsum = oddsum + n
    two_average = (evenproduct + oddsum) / 2
    difference = evenproduct - oddsum
    return difference / two_average  
integer_parameter = 5

print(strangefunction(integer_parameter))


