def factorial(n):
    fact = n                      # Set fact to passed value
    for i in range(fact, 1, -1):    # Count down from n to 1
        fact = fact * i             # Accumulate the product
    return fact

print(factorial(4))                                 # Call and print to test


