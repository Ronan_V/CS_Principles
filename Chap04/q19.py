weeklypay = 20
desiredamount = 200
neededweeks = desiredamount / weeklypay
weeksinamonth = 4
neededmonths = neededweeks / weeksinamonth
print(f"It will take {neededmonths} months to earn {desiredamount} if you make {weeklypay} dollars in a week")
