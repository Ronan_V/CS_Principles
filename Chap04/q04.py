mph = 70
mt = 140
ht = mt / mph
print(f"A car travelling at {mph} mph takes {ht} hours to go {mt} miles")

