from gasp import *
import random


begin_graphics()
done = False
junk = []

class Player:
    pass

class Robot:
    pass

def collided(player, robots):
    for robot in robots:
        if player.x == robot.x and player.y == robot.y:
            return True
    return False

def robot_crashed(bot):
    for a_bot in robots:
        if a_bot == bot:
            return False
        if a_bot.x == bot.x and a_bot.y == bot.y:
            return a_bot
    return False

def place_robots(numbots):
    global robots
    robots = []
    while len(robots) < numbots:
        robot = Robot()
        robot.x = random.randint(0,63)
        robot.y = random.randint(0,47)
        if not collided(robot, robots):
            robot.r = Box((robot.x * 10 + 5, robot.y * 10 +5), 10, 10, filled = False)
            robots.append(robot)

def place_player():
    global c, player, robots
    player = Player()
    player.x = random.randint(0,63)
    player.y = random.randint(0,47)
    while collided(player, robots) == True:
        player.x = random.randint(0,63)
        player.y = random.randint(0,47)
    c = Circle((player.x * 10 + 5, player.y * 10 +5), 5, filled = True)

def move_robots():
    global robots
    global player
    for robot in robots:
        if player.x > robot.x:
            robot.x += 1
        elif player.x < robot.x:
            robot.x -= 1
        if player.y > robot.y:
            robot.y += 1
        elif player.y < robot.y:
            robot.y -= 1
        move_to(robot.r, (robot.x * 10 + 5, robot.y * 10 + 5))

def move_player():
    global c, player
    key = update_when('key_pressed')
    while key == "r":
        remove_from_screen(c)
        place_player()
        key = update_when('key_pressed')
    if key == "w" and player.y < 47:
        player.y += 1
    elif key == "a" and player.x > 0:
        player.x -= 1
    elif key == "s" and player.y > 0:
        player.y -= 1
    elif key == "d" and player.x < 63:
        player.x += 1
    elif key == "q" and player.x > 0 and player.y < 47:
        player.x -= 1
        player.y += 1
    elif key == "e" and player.x < 63 and player.y < 47:
        player.x += 1
        player.y += 1
    elif key == "z" and player.x > 0 and player.y > 0:
        player.x -= 1
        player.y -= 1
    elif key == "c" and player.x < 63 and player.y > 0:
        player.x += 1
        player.y -= 1
    move_to(c, (player.x * 10 + 5, player.y * 10 + 5)) 

def check_collisions():
    global done, robots, junk, level
    living_robots = []
    for robot in robots:
        if collided(robot, junk):
            continue
        if collided(player, robots+junk):
            done = True
            Text("You loose!", (320, 240), size = 50)
            sleep(3)
            return
        jbot = robot_crashed(robot)
        if not jbot:
            living_robots.append(robot)
        else:
            remove_from_screen(jbot.r)
            jbot.r = Box((robot.x * 10 + 5, robot.y * 10 +5), 10, 10, filled = True)
            junk.append(jbot)
    robots = []
    for a in living_robots:
        if not collided(a, junk):
            robots.append(a)
    if not robots:
        if level != 4:
            Text(f"Level {level} complete!", (320, 240), size = 50)
            sleep(3)
            clear_screen()
            place_player()
            level += 1
            place_robots(level*5)
        else:
            Text("You win!", (320, 240), size = 50)
            sleep(3)
            return


level = 1
place_robots(5)
place_player()

while not done:
    move_player()
    move_robots()
    check_collisions()

end_graphics()
