distance = 2786.9 
mpg = 35.5
gallons = distance // mpg
cost_per_gallon = 3.65
print("Assumptions:")
print("Distance from Sacramento to Richmond: ", distance, " miles")
print("Price of gas per gallon: $", cost_per_gallon)
cost_trip = gallons * cost_per_gallon
print()
print()
print("Cost to get from Sacramento to Richmond:")
print("$", cost_trip)
#Output
#284.7
