print("To subtract two numbers in python, you have to utilize the subtraction(-) operator")
print("Subtraction is used in simple arithmetic expressions")
print(f"(7-2) is equal to {7-2} and (8-8) is equal to {8-8}")
