- Bandwidth: The amount of data that can be transmitted over a period of time. 

- Computing device: A device that is capable of performing calculations and operations. 

- Computing network: A communication system that connects multiple computing devices. 

- Computing system: A system of interconnected components that work together to achieve a common goal. 

- Data stream: A continuous flow of information, usually in the form of data packets. 

- Distributed computing system: A computing system in which multiple computers are connected and work together to handle a task. 

- Fault-tolerant: The ability of a system to continue to operate even if one or more components fail. 

- Hypertext Transfer Protocol (HTTP): A standard protocol used to communicate between web servers and web browsers. 

- Hypertext Transfer Protocol Secure (HTTPS): An encrypted version of the Hypertext Transfer Protocol that is used to protect data transmitted over the internet. 

- Internet Protocol (IP) address: A numerical label assigned to each device connected to a computer network that is used to identify and locate the device. 

- Packets: Small chunks of data that are sent across a network.

- Parallel computing system: A type of computing system which can perform multiple tasks or computations at the same time. 

- Protocols: A set of rules and conventions governing the exchange of information or data between two or more devices. 

- Redundancy: The duplication of critical components or functions of a system to increase the reliability of the system. 

- Router: A device which forwards data packets between computer networks, using a set of rules called routing protocols.

