print("In computing, the modulo operation returns the remainder or signed remainder of a division, after one number is divided by another (called the modulus of the operation).")
print("The modulo operator, denoted by %, is an arithmetic operator.")
print("Go to [wikipedia](https://en.wikipedia.org/wiki/Modulo_operation) for more information")
print(f"(5%2) is {5%2} and (4%2) is {4%2}.")

