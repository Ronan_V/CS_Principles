# CPT Scoring Summary
- To earn the maximum points (which is 6), it is required that 6 different reporting categories with 6 different scoring criterias are met. Each reporting categories and summarized scoring criterias are listed below:

## 1st Reporting Category: Program Purpose and Function
- Your video must demonnstrate the running of the program including: input, program functionality, and output.
- The written response must describe the overall purpose, functionaality, and input and output of the function.
## 2nd Reporting Category: Data Abstraction
- Your written response includes 2 program code segments, one that shows how data is stored, and another that shows data being used to fulfill the programs purpose.
- Your written response must also indentify the name of the name of the variable representing the list being utilized in this response
- Finally, your written response has to describe what the data contained in this list.
## 3rd Reporting Category: Managing Complexity
- Your written response contains a program code segment that displays a list being used to controll complexity in the program.
- Your written response must also explain how the named, selected list manages complexity in the code by explaining why the program code depends on this list.
## 4th Reporting Category: Procedual Abstraction
- Your written response includes two program segments and describes what the identified procedure does and how it contributes to the program functionality.
- One segement shows a student-developed procedure with at least one parameter that has an effect on the program functionality.
- The second segemnt shows where the procedure developed by the student is being called.
## 5th Reporting Category: Algorithm Implementation
- The written response includes a program code segemnt of an algorithm developed by a student that contains sequencing, selection, and iteration.
- The written response also explains the steps how the algorithm functions so someone else could recreate it.
## 6th Reporting Category: Testing
- The written response describes two calls to the selected procedure identified and the conditions being tested by every call to the procedure.
- The written response also identifies the product of every call.
