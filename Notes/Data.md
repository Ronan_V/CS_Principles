Abstraction: The process of selecting the most essential characteristics of an object to represent it in a simplified form.

Analog Data: Data that is continuous, representing a range of values, and is typically represented as a wave.

Bias: A prejudice or inclination in favor of one thing over another, often based on personal opinion.

Binary Number System: A number system that uses two values (0 and 1) to represent data.

Bit: The smallest unit of digital data, typically represented as a 0 or 1.

Byte: A unit of digital data consisting of eight bits.

Classifying Data: The process of sorting data into categories according to certain criteria.

Cleaning Data: The process of removing or correcting incorrect, incomplete, duplicate, or irrelevant data.

Digital Data: Data that is represented by discrete values rather than continuous values.

Filtering Data: The process of removing unwanted or irrelevant data from a data set.

Information: Data that has been processed and organized into a meaningful form.

Lossless Data Compression: A type of data compression that reduces file size without any loss of data.

Lossy Data Compression: A type of data compression that reduces file size by sacrificing some of the data.

Metadata: Data about data, such as its origin, author, date, size, and format.

Overflow Error: An error that occurs when a calculation produces a result that is too large to be stored in the given storage size.

Patterns in Data: Repeating or consistent patterns in data that can be used to identify trends or relationships.

Round-Off or Rounding Error: An error that occurs when a calculation produces a result that is slightly different from what was intended due to a limitation in precision.

Scalability: The capability of a system or process to handle increased workloads without significant changes in design or architecture.
