# Vocabulary

- asymmetric ciphers

A type of cipher that uses a public key and a private key. The public key can be utilized by anyone to encrypt a message, but only the holder of the private key can decrypt it.
- authentication

The process of verifying the identity of a user, system, or device. It is a security measure created to ensure that only entities or authorized individuals can access a special resource or system.
- bias

There are 3 types of biases in computer science: algorithmic bias, data bias, and human bias. Algorithmic bias is when an algorithm yeilds results unfair to certain groups of people. Data bias happens when the data used to train an algorithm itself is biased, therefore, producing biased results. Finally, human bias is when humans bring biases to the development and usage of algorithms and other tech.
- Certificate Authority (CA)

An organization that issues digital certificates.
- citizen science

A type of research that members of the public contribute to by collecting, analyzing, and interpreting data.
- Creative Commons licensing

A system that allows for a stadardized way for creators to give permission for the usage of their works, without the necessity of personal negotiation.
- crowdsourcing

A process in which a problem is outsourced to a collective of people, either unpaid or paid, through an open call.
- cybersecurity

The practice of protecting networks, devices, and computer systems from digital strikes, damage, and theft.
- data mining

The process of discovering trends in huge datasets through usage of algoritms and analysis.
- decryption

The conversion of encrypted data into its original form, yeilding results comprehenisble by humans.
- digital divide

The divide between households, businesses, individuals, and others that have accesible internet and other digital tech, and those who do not.
- encryption

The conversion of data into an undreadable form without a decryption key.
- intellectual property (IP)

Creations of the mind used in commerce.
- keylogging

The practice of recording the keys pressed on a keyboard.
- malware

Any software designed to harm a computer.
- multifactor authentication

A security process requiring more than one factor in order to access a resource.
- open access

The practice of making research or other scholarly arts available online, without the need for payment.
- open source

Type of software whos source code is available to the public.
- free software

Software accesible to useres without any fee or charge.
- FOSS

Free and open-source software.
- PII (personally identifiable information)

Any information that can be used to reveal the identify of an individual.
- phishing

A type of cybercrime where attackers use online channels to fool individuals into revealing sensitive information.
- plagiarism

Using others work without citation or attribution.
- public key encryption

A type of encryption that uses a public and private key, The public key being used to encrypt data, and the private key being used to decrypt it.
- rogue access point

A wireless access point set up without the knowledge of the network administrator.
- targeted marketing

Type of marketing focused on reaching a specific group of consumers.
- virus

Malware designed to replicate itself and to spread from one computer to another.
