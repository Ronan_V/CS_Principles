# Chapter 3: Names for Numbers Notes
## Assigning a Name
1. A computer can recognize a name with a value. It does this by constructing a variable.
2. An example of a variable is the score of a video game, it can vary based on what happens in the video game.
3. A variable can vary, which is why we usually call it a variable.
Think as a variable as a box that can store a value within it. This box is also labled, which is how we can associate a name with a number.
4. A computers memory comprises of patterns of voltages, but we can consider these voltages as numbers.
5. Setting a variables value is also known as an assignment.
6. There are restrictions on what you can use as a variable name, these being they must start with a letter or an underscore, it cant contain digits as the first character, it cant be a python keyword, there cant be spaces, and case matters.
### Expressions
7. The right of the assignment doesnt have to be a value, it can be an arithmetic expression such as 3 times 8.
8. Examples of expressions are: integer division which uses all the standard mathematical symbols , modulo which calculates the remainder and uses the percent symbol, and basic expressions such as 2 multiplied by 2.
9. The expression types include: addition, multiplication, integer division, division, floor division, modulo, and negation.
10. The order expressions are evaluated is the same as it is in mathematics.
11. The order goes: Negation, multiplication/ division,
#### Other Information
12. Assignment Dyslexia: Assignment Dyslexia is placing the value on the left and the variable name on the right. This is not a valid or legal statement.
13. Tracing: Tracing a program means to keep track of the variables in said program to see how their values fluctuate as statements are executed.
14. In chapter 3, Code lens was utilized to trace a program. 
