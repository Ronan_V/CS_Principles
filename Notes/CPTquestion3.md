# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
>
>The overall purpose of this function is to create a game in which the player is meant to dodge incoming robots in order to win.
>
2. Describes what functionality of the program is demonstrated in the
   video.
>
>The video demostrantes the user utilizing the controls to move up, down, lef, right, and diagonally. The video also demonsrates the usage of the teleport key, and the evasion of incoming robots. 
>
3. Describes the input and output of the program demonstrated in the
   video.
>
The user presses a key as an input, the output is where the player moves, and where the robots move. This is demonstrated in the video when the player moves around under constant pursuit of the robots.
>

## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
>
>["KP_2", "x", "comma"]
>
2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.
>
>elif key in ["KP_2", "x", "comma"] and player.y > 0:
        player.y -= 1
>

## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
>
>The name for this list will be called "key_pressed".
>
2. Describes what the data contained in the list represent in your
   program
>
>The data contained in the list displays the possible keys the user can press in order to move downwards.
>
3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list
>
>
>

## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
>
>def collided(thing1, list_of_things):
    for thing2 in list_of_things:
        if thing1.x == thing2.x and thing1.y == thing2.y:
            return True
    return False
>
>
>
2. The second program code segment must show where your student-developed
   procedure is being called in your program.
>
>if collided(robot, junk):
>
>

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
>
>The procedure checks for collisions by taking a variable and a list, then determines whether the variable and the list are located in the same spot.
>
>
4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
>
>First, the procedure takes 2 parameters, a variable and a list. It then repeats through the list, checking if the x's and y's are the same. Furthermore, the function then returns false if there are no collisions, and true if there are.
>
>

## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.
>
> First call:
>
>if collided(robot, junk):
>
>
> Second call:
>
>if collided(player, robots):
>
>
2. Describes what condition(s) is being tested by each call to the procedure
>
> Condition(s) tested by the first call:
>
>A robot, and two robots crashed into eachother (junk).
>
>
> Condition(s) tested by the second call:
>
>The user/player, which is represented by a black circle. The list of all robots.
>
>
3. Identifies the result of each call
>
> Result of the first call:
>
>Depends on if true or false, false if no collisions were made, and true if there were.
>
>
> Result of the second call:
>
>Also depends on if true or false. 
>
>
