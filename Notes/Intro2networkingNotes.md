# Chapter One
- The passage discusses how people communicate with each other, using the example of five people in a room and how they can talk to each other. It then poses the question of how people can communicate with each other if they are in different rooms, which leads to the development of telephone systems.
- Early telephone systems used wires, microphones, and speakers, and required human operators to connect wires between pairs of people who wanted to talk to each other. Local calls were generally less expensive than long-distance calls because of the cost of maintaining and installing the wires.
- As telephone companies started using fiber optic, more advanced techniques were developed to carry many simultaneous long-distance conversations on a single fiber.
- The passage also talks about how computers communicate with each other, and how in the earliest days, computers were connected with wires. The owners of computers generally had to lease wires from telephone companies to connect their computers, and the leased lines were quite expensive because they were used 24 hours a day.
- In the 1970s and 1980s, people working at universities around the world wanted to send each other data and messages using computer-to-computer connections, which were expensive to maintain.
# Chapter Two

The Internet was built by breaking down the overall problem into four basic subproblems that could be worked on independently by different groups, which were named (1) Link, (2) Internetwork, (3) Transport, and (4) Application. These are visualized as layers stacked on top of each other, with the Link layer at the bottom and the Application layer at the top. The Link layer is responsible for connecting a computer to its local network and moving the data across a single hop, using technologies that can be shared among multiple computers at the same location. The Link layer needs to solve two basic problems when dealing with these shared local area networks, which are how to encode and send data across the link and how to cooperate with other computers that might want to send data at the same time. The engineers designed an ingenious method called "Carrier Sense Multiple Access with Collision Detection" (CSMA/CD) to solve the problem of sharing the network. CSMA/CD allows each computer to wait its turn to use the shared network in a fair way. When only one computer wants to send data, it sends packets one right after another and moves its data across the network as quickly as it can. But if multiple computers want to send data at the same time, each computer sends one packet and then waits while the other computers send packets, sharing access to the network in a fair way.

# Chapter Three

The passage describes the lowest layer of the Internet Architecture, known as the Link layer, which is responsible for transmitting data using physical media like wires, fiber optic cables, or radio signals. The Link layer is closer to the physical network media, and it transmits data only part of the way from the source computer to the destination computer. To reach the ultimate destination, packets are forwarded across multiple links.

WiFi is an example of a link layer that can transmit data up to a kilometer, and the first router that handles a computer's packets is called the "base station" or "gateway." All computers within range of the base station can hear all packets sent by the base station and every other nearby computer, and therefore, each computer needs to know which packets to treat as its own and which packets to ignore.

Each WiFi radio in every device is given a unique serial number, called a MAC address, at the time of manufacture. Every packet sent across the WiFi has a source and destination address, which is like a "from" or "to" address on a postcard, so all computers know which messages are theirs. When a computer connects to a new WiFi network, it must discover the MAC address for the gateway of that particular WiFi by sending a broadcast message to a broadcast address.

# Chapter 4 

The passage discusses how data moves across multiple networks and hops to reach its destination. Just as when a person travels to a distant destination, the packet also takes different forms of transportation like WiFi, Ethernet, fiber optic, and satellite across different link layers to reach its destination.

Routers play a crucial role in ensuring that packets are forwarded to the correct outbound link layer. Unlike a train station, routers don't require passengers to look at displays to figure out their next train because every packet is marked with its ultimate destination address, known as the Internet Protocol (IP) Address. IP addresses are constructed carefully to assign another address to every computer based on where it is connected to the network.

There are two versions of IP addresses, the old IPv4, and the longer IPv6. The IPv4 addresses consist of four numbers separated by dots, and each number can only be from 0 through 255. However, we are running out of IPv4 addresses, and therefore IPv6 addresses are being used more widely.

IP addresses can be broken into two parts, the network number and the host identifier. The network number is used to connect many computers to the internet via a single connection. For example, a college campus, school, or business could connect using a single network number, and all computers connected to that network appear to the rest of the internet on a single connection. By using this approach of a network number and a host identifier, routers do not have to keep track of billions of individual computers but only need to keep track of a million or less different network numbers.

The gateway (often called a router) responds to the broadcast message with its IP address and a lease time, which is the amount of time the router is willing to let the computer use that IP address before it has to ask for a new one. The computer then uses that IP address until the lease time expires, or until it moves to a different network.

DHCP is commonly used in homes and offices where computers are moved from one network to another, but it is also used in larger networks like airports, hotels, and coffee shops. When you connect to the WiFi at a coffee shop, your computer sends a DHCP request asking for an IP address. The coffee shop’s router assigns your computer an IP address and a lease time, which lets you access the Internet until you leave the coffee shop.

DHCP can also assign other information to your computer, like the IP addresses of Domain Name System (DNS) servers. DNS servers translate domain names (like google.com) into IP addresses (like 172.217.7.14) so that your computer knows where to send packets. DHCP can also assign other configuration information like the subnet mask and the default gateway address.

In summary, DHCP is a protocol that allows computers to obtain IP addresses dynamically, making it easy for mobile devices to connect to different networks. It is commonly used in homes, offices, and public places like airports and coffee shops. DHCP can also assign other important configuration information like DNS server addresses, subnet masks, and default gateway addresses.

# Chapter 5

Main points:

DNS allows accessing websites by their domain name instead of IP address.
IP address is determined by the location where the computer connects to the Internet and may change.

Looking up the IP address for a DNS address makes it easier to move a server from one location to another.

ICANN is the organization that chooses top-level domains (TLDs) and assigns them to other organizations to manage.

Two-letter country code top-level domain names (ccTLDs) are assigned to countries, and policies for applying for domain names with any particular ccTLD vary widely.

Once a domain name is assigned to an organization, the controlling organization can assign subdomains within the domain.

The left prefix of an IP address is the network number, and the right part is the most specific.

For domain names, the most general part is on the right, and the subdomains become more specific as you move left.

# Chapter 6

The Transport layer is responsible for ensuring reliable transmission of data across the network.

The Internetworking layer does not guarantee delivery of any particular packet, and packets can be lost or misrouted.

The Transport layer handles reliability and message reconstruction on the destination computer.

The Transport layer adds a small amount of data to each packet to help solve the problems of packet reassembly and retransmission.

A packet contains a link header, an IP header, a Transport Control Protocol (TCP) header, and the actual data.

The link header is removed when the packet is received on one link and a new link header is added when the packet is sent out on the next link.

The IP header holds the source and destination IP addresses and the Time to Live (TTL) for the packet.

The TCP headers indicate where the data in each packet belongs, and the source computer keeps track of the position of each packet relative to the beginning of the message or file.

The destination computer looks at the offset position to put the packet into the proper place in the reassembled message.

The Transport layer handles packets that arrive out of order by placing them in a buffer and fitting them into the gap in the reassembled data.

The sending computer only sends a certain amount of data before waiting for an acknowledgment from the receiving computer that the packets were received, which is called the "window size."

The sending computer adjusts the window size to send data quickly over fast connections that have light loads and to avoid overwhelming the network.

If a packet is lost, the destination computer sends a packet to the sending computer indicating where in the stream the receiving computer has last received data, and the sending computer resends data from the last position that the receiving computer had successfully received.

The Transport layer continuously monitors how quickly it receives acknowledgments and dynamically adjusts its window size.

The sending computer must hold on to all of the data it is sending until the data has been acknowledged.

# Chapter 7

The Application layer is the top layer of the four-layer TCP/IP network model, where networked software like web browsers, mail programs, video players, or networked video players operate, and users interact with them while the applications interact with the network on their behalf.

Networked applications require two parts, the client and the server, which use the Transport layer on each of their computers to exchange data in a client/server architecture.

When browsing a web address, a web application running on the user's computer connects to the appropriate web server, retrieves pages for the user to view, and then shows them.

A set of rules that govern how we communicate is called a "protocol," and the protocol between web clients and web servers is the "HyperText Transport Protocol" or HTTP for short, which is described in a number of large documents starting with RFC7230.

HTTP is relatively simple compared to most client/server protocols, but there is a lot of detail that allows web clients and servers to communicate efficiently and transfer a wide range of information and data.

The telnet application was first developed in 1968 and is one of the earliest Internet applications ever built, according to one of the earliest standards for the Internet. It is still present in some systems today.

The HTTP protocol is used to transfer data over the internet and it is only one of many client/server application protocols used on the Internet. Another common protocol is used so that a mail application running on your computer can retrieve mail from a central server. The Internet Message Access Protocol (IMAP) is a more complicated protocol than the web protocol, so we won’t be able to use the telnet command to fake the protocol. If someone wants to develop a mail reading application, they could carefully read the IMAP document and develop code to have a successful conversation with a standards-compliant IMAP server. The messages that are sent by the client and server are precisely formatted and are sent in a specific order so that they can be generated and read by networked computer applications on each end of the connection. The messages are not designed to be viewed by an end-user and are not particularly descriptive.

## Chapter 8

The passage discusses the evolution of network security, starting with the early days of the Internet, where networks were small, and all routers were in secure locations, and security of data crossing the network was not a concern. However, as the use of the Internet grew, security and privacy of network traffic became important problems to solve, especially with the introduction of wireless technologies like Wi-Fi. The two general approaches to securing network activity are physically securing all network hardware or encrypting data in a computer before sending it across its first physical link. The concept of protecting information during transport over an insecure medium is thousands of years old, and the simplest version of this approach is to encrypt data using a code called the "Caesar Cipher," where characters are shifted a fixed distance down the alphabet to produce the scrambled message or "ciphertext." Modern encryption techniques are far more sophisticated than a simple character shift, but all encryption systems depend on some kind of a secret key that both parties are aware of so they can decrypt received data. The traditional way to encrypt transmissions is using a shared secret that only the sending and receiving parties know. However, this approach cannot scale to situations where a company might have millions of customers. The solution to this problem came in the 1970s when the concept of asymmetric key encryption was developed, where one key is used to encrypt the message, and another key is used to decrypt it.
