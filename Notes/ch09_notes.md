# Using Repitition with Strings
1. Python already has the integrated abiltiy to tinker with words or strings.
2. A string is a collection of digits, letters, or other characters.
3. A python for loop knows how to step through letters.
4. The addition symbol (+) appends strings together.
5. The same accumulator pattern works.
6. As a small reminder, here are all five steps to take in the accumulator pattern:
- Set the accumulator variable to its initial value. This is the value we want if there is no data to be processed.
- Get all the data to be processed.
- Step through all the data using a for loop so that the variable takes on each value in the data.
- Combine each piece of the data into the accumulator.
- Do something with the result.
# Reversing Text
1. You can reverse text by altering step 4 in the accumulator pattern, combining before rather than afterward.
2. ![Image of Question](Screen%20Shot%202022-11-03%20at%204.34.47%20PM.png)
# Mirroring Text
1. We can mirror text by adding the letter to both sides of the new string that we are creating
2. ![Image of Question 2](Screen%20Shot%202022-11-03%20at%204.43.24%20PM.png)
# Modifying Text
1. We can loop through the string and change or modify it by utilizing find and slice (substring).
2. ![Image of Question 3](Screen%20Shot%202022-11-03%20at%204.46.16%20PM.png)
# Chapter Summary
- Chapter 9 included the following concepts from computing:

1. Accumulator Pattern - The accumulator pattern is a set of steps that processes a list of values. One example of an accumulator pattern is the code to reverse the characters in a string.
2. Palindrome - A palindrome has the same letters if you read it from left to right as it does if you read it from right to left. An example is "A but tuba".
3. String - A string is a collection of letters, numbers, and other characters like spaces inside of a pair of single or double quotes.

