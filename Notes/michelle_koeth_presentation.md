# Notes:

IBM calls edge computing: “computing that acts on data at the source.”

Edge computing collects and processes data at the source. It has less central processing and less network traffic and thus less latency.

The Raspberry Pi and Esp32 are both examples of edge computing. Raspberry Pi has it’s own operating system whereas Esp32 does not. Esp32 utilizes embedded coding which increasing the efficiency of data processing. Esp32 has a less powerful processor than Raspberry Pi.

Some benefits of microprocessors based on machine learning are less power usage, always-on use case, and is more compact.

You can train machine learning to recognize a species of birds by showing it images of birds.

Machine learning is the ability for a computer to program itself.

ChatGPT is an example of neural networks.

Convolution is utilized to develop filters for machine learning.

To create a compact esp32 board, there are 2 methods: Quantification and Pruning.

Edge Impulse simplifies machine learning to make it very easy to use.

Mobilenet V1 is the TinyML model architecture. 

XXD is a hex editor.

# Comments:

8 dollars is pretty cheap for an esp32 cam.

Fuzzy logic is a goofy term. It seems quite outdated.

I enjoyed learning about high-level calculus terms that relate to edge computing and machine learning.

I am interested utilizing edge computing boards for recreational purposes.

It is cool to see which birds the computing model is able to efficiently recognize and efficiently recognize.

# Questions:

What other images can machine learning recognize?

What is convolution?

What is transfer learning in depth?

What other birds was the computing model unable to recognize efficiently?

What other birds was the computing model able to easily recognize?

