# Repeating Steps

1. Use a "for" loop to repeat code.
2. Use range to make a list of numbers.
3. A computer can repeat the same process without any mistakes.
4. A computer can execute a program as long as it has electricity.
5. The way computers repeat steps is called a "loop" or "iteration".

## Repeating with Numbers
1. A for loop is a type of loop that allows you to repeat a statement or set of statements in a program.
2. A for loop will use a variable which will take on each of the values on a list one at a time.
3. A list has all values in an order.
4. : and an indentation are bot required for a loop.
5. The body of the loop is repeated for each value in a list.

### What is a List?
1. A list hold items in order.
2. In python, a list is enclosed with brackets [].
3. Values in a list can be seperated with commas.
4. A list contains an order and each item on the list has a postion in the list, such as the 1st item or the last item.
5. Using better variable names is very helpful and required if you want to have code that is easy to read.

#### The Range Function
1. The range fucntion is used to loop over a sequence of numbers.
2. If the range fuction is called with a single positive integer, it will generate all the integer values from 0 to 1 less than the number it was passed and assign them one at a time to the loop variable.
3. If 2 integer values are inputted into the range function, then it will generate each of the integers coordinating to the first value to one less than the second value.
4. It is exclusive of the 2nd value, but inclusive of the 1st.
5. For example, range(1, 10) generates integers from 1 to 9.

##### Pattern
1. There is a pattern in all of these programs, which is a common pattern found when processing data.
2. This pattern is known as the "Accumulator Pattern".
3. In programs, we accumulate values into variables with said patetrn.
4. According to the text there are 5 steps to this pattern.
5. Here are the five steps in this pattern:

1. Set the accumulator variable to its initial value. This is the value we want if there is no data to be processed.
2. Get all the data to be processed.
3. Step through all the data using a for loop so that the variable takes on each value in the data.
4. Combine each piece of the data into the accumulator.
5. Do something with the result.

###### Adding Print Statements
1. A print statement is a prediction about variable values, and is used to see if said prediction is correct.
2. Keywords:
def - The def keyword is used to define a procedure or function in Python. The line must also end with a : and the body of the procedure or function must be indented 4 spaces.
for - A for loop is a programming statement that tells the computer to repeat a statement or a set of statements. It is one type of loop.
print - The print statement in Python will print the value of the items passed to it.
range - The range function in Python returns a list of consecutive values. If the range function is passed one value it returns a list with the numbers from 0 up to and not including the passed number. For example, range(5) returns a list of [0, 1, 2, 3, 4]. If the range function is passed two numbers separated by a comma it returns a list including the first number and then up to but not including the second number. For example, range(1, 4) returns the list [1, 2, 3]. If it is passed three values range(start,end,step) it returns all the numbers from start to one less than end changing by step. For example, range(0, 10, 2) returns [0, 2, 4, 6, 8].
