1. How is the APCSP exam scored? How many of the 70 multiple choice questions do I need to get correct to earn a 5, 4, or 3 on the exam? What is the Create Performance Task and how is it scored?

The APCSP exam is scored on a scale of 1 to 5. Assuming you get a 6/6 on the Create Performance Task, you need a 60/70 to get an 5, a 51/70 to get a 4, and a 32/70 to get a 3. The Create Performance Task is a task that focuses specifically on the creation of a computer program, including a written response and a video.It is scored out of 6.

2. On which of the 5 Big Ideas did I score best? On which do I need the most improvement?

I scored best on Data. I need the most improvement on computer system and networks.

3. What online resources are available to help me prepare for the exam in each of the Big Idea areas?

Useful Resources:

[score calculator](https://www.albert.io/blog/ap-computer-science-principles-score-calculator/)

[princeton review](https://www.princetonreview.com) 
