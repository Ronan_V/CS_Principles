# Notes

- AI affects how we abuse technology by: reduced cost, automation of human activity, new threats, fine targeting, and scaling to non-deterministic systems.

- Neural networks were initially developed to mimic how the human brain functions.

- The reason why algorithms like GPT takes an insane amount of computations because it involves a massive amount of waits utilizing a massive neural network.

- The tradeoff of convolutions is that it gives an image with lower quality but makes the process less complicated.

- You can unconvolute someting but it is undecideable. Mr. Mickelson compares unconvoluting something to unscrambling an egg.

- Tensor flow is a good application to use in order to experiment with neural networks.

- For recurrant neural networks, all you need is the first input or seed in order to start.

- RNN will probabalistically generate new content.

- RNN is unreasonably effective on generating something that looks really good.

- LSTM stands for long short term memory.

- The neural network will remember what happened before it, because the output is the input for the next step.

- Any translation application is trained to match sequences.

- The translation application cannot translate slang.  

- ChatGPT doesn't process your input character by character, but processes it all at once.

- ChatGPT is basically a glorified auto-complete.

- ChatGPT is an undeterministic system because if you input the same question 2 times, it will give a different output each time.

- AI will sometimes get calculations incorrect because it is an undeterministic system so its responses will sometimes be lies.

- It's not safe to keep the human in the system and it's not safe to let the system run itself.

- The most valuable models are testable, understandable, verifiable, and probabalistic.

- Make sure to have ground truth before making and testing a predictive algorithm.

- Rare events actually happen all the time.

# Questions

- What is the definition of convolution?

- What is the definition of transfer learning?

- What does it mean for something to be unreasonably effective?

- Why is it called schrodinger's accuracy?

- Can you use ChatGPT to create functional programs in different languages?
