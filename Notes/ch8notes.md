# Loops - For and While
1. We have been using the for loop to repeat the body of a loop a known number of recursions.
2. Be sure to indent 4 spaces when making a loop.
3. We can print a count that starts at 1 and increases by 1 every time by using a for loop.
4. The while loop is another way to repeat expressions.
5. It will repeat the body of the loop as long as a logical expression is valid.
6. The body of the loop is the part of the loo that is indented 4 or more spaces to the right of the while loop.
7. A logical expression is an expression that is either true or false, for example x > 3.
8. An example of a while loop is musical chairs, because you are circling around the chairs while the music is playing.
